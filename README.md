# CoderDojo-CPP

Un curs de C++ pentru ninjalăii de la CoderDojo. Pentru vârstele de la 9 ani în sus.

Pentru a compila este necesar http://asciidoctor.org.
Se compilează în HTML cu comanda:

    asciidoctor -b html -D ./build Numere.adoc


# De făcut:

    * Operatorii de incrementare, decrementare
    * structura de bază a unui program C++
    * Descriere `cout` și `cin` cu operatorii `<<` respectiv `>>`
    * Descriere concept variabilă
    * Introducere în condiții și verificarea lor

## Note pentru „De făcut”

```
#include <iosteam>
using namespace std;
int main ()
{
    int cutie;
    cutie = 6;
    if (cutie == 3)
        cout << "Mere" << endl;         // nu se printează
    cout << "Cățel" << endl;             // se printează

    cutie = 3;
    if (cutie >= 3)
    {
        cout << "Castravete" << endl;   // nu se printează
        cout << "Dovleac" << endl;      // nu se printează
    }
    cout << "Pisicuță" << endl;         // se printează

    if (cutie != 6)
    {
        cout << "Căluț" << endl;        // se printează
    }

    return 0;
}
```
